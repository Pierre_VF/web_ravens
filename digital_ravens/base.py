import re
from typing import Optional
from urllib.parse import urljoin, urlparse

import requests
from bs4 import BeautifulSoup


def sentence_from_element(x: str) -> str:
    return " ".join(x.text.split())


def _is_valid_sub_url(url: str) -> bool:
    if url.startswith("javascript:"):
        return False
    elif url.endswith(".css"):
        return False
    else:
        return True


def get_interesting_page_contents(url: str) -> dict[str, list[str]]:
    page = requests.get(url)
    soup = BeautifulSoup(page.content, "html.parser")

    out = {}
    for field_type in ["h1", "h2", "h3", "p"]:
        l = []
        xs = soup.find_all(field_type)
        for p in xs:
            s_i = sentence_from_element(p)
            l.append(s_i)
        out[field_type] = l

    xs = soup.find_all("a", href=True)
    internal_links = []
    external_links = []
    email_addresses = []
    for p in xs:
        s_i = p["href"]
        if s_i.startswith("mailto:"):
            email_addresses.append(s_i.replace("mailto:", ""))
        elif s_i.startswith("http") or s_i.startswith("ftp"):
            external_links.append(s_i)
        else:
            local_link = urljoin(url, s_i)
            internal_links.append(local_link)

    out["links"] = {
        "external": external_links,
        "internal": internal_links,
        "emails": email_addresses,
    }

    return out


class BasicCrawler:
    def __init__(self):
        self.__already_crawled: list[str] = []
        self.__remaining_max_pages: Optional[int] = None
        self.__stack_to_crawl: list[str] = []

    def add_to_crawled(self, url: str) -> None:
        self.__already_crawled.append(url)

    def has_already_crawled(self, url: str) -> bool:
        return url in self.__already_crawled

    def is_relevant_url(self, url: str) -> bool:
        if self.has_already_crawled(url):
            relevant = False
        elif url.startswith("javascript:") or url.endswith(".css"):
            relevant = False
        elif url.endswith(".pdf") or url.endswith(".docx") or url.endswith(".pptx"):
            relevant = False
        else:
            relevant = True
        return relevant

    def _add_to_stack(self, url: str):
        url_i = urlparse(url)._replace(fragment="").geturl()
        if url in self.__stack_to_crawl:
            pass
        elif self.is_relevant_url(url_i):
            self.__stack_to_crawl.append(url_i)

    def _relevant_referred_urls(
        self,
        res,
        current_url: str,
        restrict_to_internal: bool = False,
        add_to_stack: bool = True,
    ) -> list[str]:
        current_domain = urlparse(current_url).hostname

        if restrict_to_internal:
            get_further_to = res["links"]["internal"]
        else:
            get_further_to = res["links"]["external"] + res["links"]["internal"]

        out = []
        for j in get_further_to:
            if self.has_already_crawled(j):
                # Skip urls already crawled
                pass
            elif restrict_to_internal and (current_domain != urlparse(j).hostname):
                # Skip external urls
                pass
            else:
                if add_to_stack:
                    self._add_to_stack(j)
                out.append(j)
        return out

    def crawl(
        self,
        url: str,
        max_pages: Optional[int] = None,
        restrict_to_internal: bool = False,
    ) -> list:
        self._add_to_stack(url)
        out = []
        pages_crawled = 0
        while len(self.__stack_to_crawl) > 0:
            if max_pages and pages_crawled >= max_pages:
                break
            i = self.__stack_to_crawl.pop(0)
            pages_crawled += 1
            print(f"Crawling [{pages_crawled}]:  {i}")
            try:
                self.add_to_crawled(i)
                y = get_interesting_page_contents(i)
                out.append(y)
                self._relevant_referred_urls(
                    y,
                    current_url=i,
                    restrict_to_internal=restrict_to_internal,
                    add_to_stack=True,
                )
            except:
                print("error on: " + i)

        return out

    def recursive_crawl(
        self,
        url: str,
        depth: int = 2,
        max_pages: Optional[int] = None,
        restrict_to_internal: bool = False,
    ):
        if self.__remaining_max_pages is not None:
            if self.__remaining_max_pages < 1:
                return []
            else:
                self.__remaining_max_pages -= 1
        if max_pages:
            self.__remaining_max_pages = max_pages - 1
        out = []
        print("Crawling: " + url)
        y = get_interesting_page_contents(url)
        self.add_to_crawled(url)
        out.append(y)
        if depth > 0:
            urls = self._relevant_referred_urls(
                y,
                current_url=url,
                restrict_to_internal=restrict_to_internal,
                add_to_stack=False,
            )
            for i in urls:
                if self.is_relevant_url(i):
                    try:
                        out += self.recursive_crawl(i, depth=depth - 1)
                    except:
                        print("error on: " + i)
        return out


def list_of_unique_elements(x: list) -> list:
    return list(set(x))


def extract_email_address(x):
    return re.search(r"[\w\.-]+@[\w\.-]+[a-z]", x).group(0)
