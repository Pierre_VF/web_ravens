from digital_ravens.base import BasicCrawler, list_of_unique_elements


def check_url(
    url, look_for: str, max_pages: int = 10, restrict_to_internal: bool = False
) -> list:

    crawler = BasicCrawler()

    out = []
    if look_for == "emails":
        res = crawler.crawl(
            url, max_pages=max_pages, restrict_to_internal=restrict_to_internal
        )
        for y in res:
            out += y["links"]["emails"]
        out = list_of_unique_elements(out)
    elif look_for == "external_links":
        res = crawler.crawl(
            url, max_pages=max_pages, restrict_to_internal=restrict_to_internal
        )
        for y in res:
            out += y["links"]["external"]
        out = list_of_unique_elements(out)
    elif look_for == "internal_links":
        res = crawler.crawl(url, max_pages=max_pages, restrict_to_internal=True)
        for y in res:
            out += y["links"]["internal"]
        out = list_of_unique_elements(out)
    elif look_for == "*":
        out = crawler.crawl(
            url, max_pages=max_pages, restrict_to_internal=restrict_to_internal
        )
    else:
        raise ValueError(f"Illegal scope ({look_for})")
    return out


if __name__ == "__main__":
    out = check_url("https://dtime.ai/", look_for="external_links", max_pages=30)
    # out = check_url("https://www.aau.dk/", look_for="emails", max_pages=25)
    print("abc")
